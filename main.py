import argparse
import clashroyale

import time
from datetime import datetime, timedelta

import requesting

indiv = ['PvP', 'clanMate', 'challenge', 'friendly', 'casual1v1']

# Receives raw data of a battle from the API and 
# the list where will be stored the data 
def parseBattle(data, dst, batType):
    # Check if individual or 2v2 battle
    # batType == True when desired 2v2
    if batType == False:
        # If battle type is not one of the indiv's, it's a 2v2, so return
        if data['type'] not in indiv:
            return 0
    else:
        if data['type'] in indiv:
            return 0
    
    # We extract battle's time
    date = data['battleTime'][0:15]
    date = date.replace('T', '')
    date = datetime.strptime(date, '%Y%m%d%H%M%S') + timedelta(hours=1)
    
    # We will only check last 2 hours battles
    # Looking for any lost battle in the output
    if date + timedelta(hours=20) > datetime.now():
        bat = ''
        bat += str(date) + ',' + data['type'] + ',' + data['gameMode']['name'] + ','
        # Player 1 data
        for item in data['team']:
            # If player doesn't belong to a clan, will produce an exception
            try:
                bat += item['name'] + ',' + item['tag'] + ',' + item['clan']['name'] + ',' + item['clan']['tag'] + ',' + \
                    str(item['crowns']) + ',' 
            except:
                bat += item['name'] + ',' + item['tag'] + ',' +  'None,' + 'None,' + \
                    str(item['crowns']) + ',' 

            # King's and Princesses' Towers needs special treatment            
            try:
                bat += str(item['kingTowerHitPoints']) + ','
            except:
                bat += '0,'
            
            try:
                n = len(item['princessTowersHitPoints'])
                if n == 1:
                    bat += str(item['princessTowersHitPoints'][0]) + ',0,'
                else:
                    bat += str(item['princessTowersHitPoints'][0]) + ',' + str(item['princessTowersHitPoints'][1]) + ','
            except:
                bat += "0,0,"
            
            # Finally, the deck's cards
            deck = []
            for card in item['cards']:
                deck.append(card['name'])
            
            deck.sort()
            
            for card in deck:
                bat += card + ','
                
        # Player 2 data
        for item in data['opponent']:
            # If player doesn't belong to a clan, will produce an exception
            try:
                bat += item['name'] + ',' + item['tag'] + ',' + item['clan']['name'] + ',' + item['clan']['tag'] + ',' + \
                    str(item['crowns']) + ',' 
            except:
                bat += item['name'] + ',' + item['tag'] + ',' +  'None,' + 'None,' + \
                    str(item['crowns']) + ',' 
            

            # King's and Princesses' Towers needs special treatment
            try:
                bat += str(item['kingTowerHitPoints']) + ','
            except:
                bat += '0,'
            
            try:
                n = len(item['princessTowersHitPoints'])
                if n == 1:
                    bat += str(item['princessTowersHitPoints'][0]) + ',0,'
                else:
                    bat += str(item['princessTowersHitPoints'][0]) + ',' + str(item['princessTowersHitPoints'][1]) + ','
            except:
                bat += "0,0,"
            
            # Finally, the deck's cards
            deck = []
            for card in item['cards']:
                deck.append(card['name'])
            
            deck.sort()
            
            for card in deck:
                bat += card + ','
        dst.append(bat)
        return 0
    else:
        # API returns battles since last, so if date is bigger than 
        # 2 hours, the next will also be old -> continue with other player
        return 1
        




if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description = 'Process ClashRoyale API player battles request')
    
    # File containing players' tags
    parser.add_argument('-p', dest = 'players', action = 'store', required = True)
    
    # File with API token
    parser.add_argument('-t', dest = 'fileToken', action = 'store', required = True)
    
    # File to add battles
    parser.add_argument('-o', dest = 'output', action = 'store', default = 'output.csv')

    # Async or sync request
    parser.add_argument('-a', dest = 'asyn', action = 'store_true', default = False)

    # 2v2 or individual matches
    parser.add_argument('-c', dest = 'couples', action = 'store_true', default = False)
    
    args = parser.parse_args()
    
    # Read API token
    tok = open(args.fileToken, 'r+')
    token = tok.readlines()[0].replace('\n', '')
    tok.close()
    
    # Read players' tags
    tag = open(args.players, 'r+')
    tags = tag.readlines()[0]
    tags = tags.split(' ')
    tag.close()
    
    # To store all minned battles 
    newBattles = []
    

    if args.asyn == True:
        requesting.async_request(tags, token, newBattles, args.couples)
    else:
        requesting.sync_request(tags, token, newBattles, args.couples)


    # We read out output file to avoid adding already present matches
    # If file exists, we have to compare newBattles with existing ones (oldBattles)
    try:
        out = open(args.output, 'r+')
        oldBattles = out.readlines()[1:]
        out.close()
        
        # Stored battles are ended with '\n', so we have to delete this character to compare
        oldBattles = [b.replace('\n', '') for b in oldBattles]
        
        # We select new battles which doesn't exist in the output file
        addingBattles = [x for x in newBattles if x not in oldBattles]

    # If file doesn't exist, it will add all new battles
    except:
        addingBattles = newBattles

    # Open output for writing
    out = open(args.output, 'a+')
    # Write battles one by one
    for line in addingBattles:
        out.write(line + '\n')
    out.close()
    
    # Tell how many battles we added
    print('Added ' + str(len(addingBattles)))

    
