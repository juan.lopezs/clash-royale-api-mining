# Sync request, using clashroyale wrapper
import clashroyale
import main


# Async request uses 
import asyncio
import aiohttp

import json


# 
def sync_request(tags, token, dst):
    # Initializing API client
    client = clashroyale.official_api.Client(token)
    
    # Requesting data for each player
    for t in tags:
        data = client.get_player_battles(t.replace('\n', ''))
        # Minning each battle
        for item in data:
            if main.parseBattle(item, dst) != 0:
                break






# Async requests
async def fetch(url, session):
    async with session.get(url) as response:
        return await response.read()

async def run(players, dst, headers):
    url = "https://proxy.royaleapi.dev/v1/players/%{}/battlelog"
    tasks = []

    # Limit rate
    connector = aiohttp.TCPConnector(limit=15)

    # Fetch all responses within one Client session,
    # keep connection alive for all requests.
    async with aiohttp.ClientSession(headers=headers, connector=connector) as session:
        for i in range(len(players)):
            task = asyncio.ensure_future(fetch(url.format(players[i]), session))
            tasks.append(task)

        responses = await asyncio.gather(*tasks)
        
        for item in responses:
            for b in json.loads(item):
                
                if main.parseBattle(b, dst) != 0:
                    break
            

def async_request(tags, token, dst):
    
    headers = {
    'Authorization' : "Bearer " + token
    }

    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(run(tags, dst, headers))
    loop.run_until_complete(future)
