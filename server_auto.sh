#/bin/bash -u

# Our file name will be data-M.YY.csv
now="$(date +'%m.%y')"
name="data-"
name="$name$now.csv"

# If file doesn't exist [new month], create it and add dataframe's headers
if ! test -f $name
then
    cat headers.txt > $name
fi

# Execute program
python main.py -p players.txt -t token-proxy.txt -o $name -a
