# Clash Royale API mining

Clash Royale mining program to extract players battles data.


## Requirements 📋

The lonely library you must install is _clashroyale_. You can do it with _pip_ as follows:

```
pip install clashroyale
```


## Executing the program ⚙️

In the command line, type:
```
main.py [-h] -p PLAYERS -t FILETOKEN [-o OUTPUT] [-a] [-d]
```
The opt h shows program help. 
You must specificy opts p and t. Their value (PLAYERS and FILETOKEN) represent the file which contains players' tags (must be separated by white spaces, in only 1 line; see _players.txt_) and the one that contains your API token (raw text).  
By default, output file will be _output.csv_, but you can specify a given file to store all data.
_-a_ is used to make the query asynchronously (very recommended because of speed).
_-d_ represents duo's matches (2v2).  

Note that async requests uses the RoyaleAPI proxy, so it's different token as in sync, which uses the Official API by clashroyale python wrapper (made by cgrok, see https://github.com/cgrok/clashroyale).